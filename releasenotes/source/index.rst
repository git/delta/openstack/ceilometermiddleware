====================================
 Ceilometer Middleware Release Notes
====================================

 .. toctree::
    :maxdepth: 1

    unreleased
    zed
    yoga
    xena
    wallaby
    victoria
    ussuri
    train
    stein
    queens
    pike
    ocata
    newton
    mitaka
